Flask~=2.0.0
flask-sitemap~=0.3.0

# iicalc plugin backend
setuptools~=57.4.0
requests~=2.25.1
requests-oauthlib~=1.3.0
flask-mail~=0.9.1
azure-storage-file-share~=12.5.0

# heroku
gunicorn~=20.1.0