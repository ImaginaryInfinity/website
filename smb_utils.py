"""Module used to get, put, and delete files from the smb server"""
import os
import re
import pathlib

from azure.storage.fileshare import ShareFileClient, ShareDirectoryClient

secure_folder = pathlib.Path(__file__).parent / 'secure'
with open(secure_folder / 'smb_connection_string.txt') as f:
	connection_string = f.read().strip()


def smb_get(filename, to_dir="."):
	"""Get a file from the smb server, and store it in ```to_dir```"""
	current_pwd = os.getcwd()
	os.chdir(to_dir)
	directory, filename = os.path.split(filename)
	dir_service = ShareDirectoryClient.from_connection_string(
		conn_str=connection_string, share_name="website", directory_path=directory
	)
	allfiles = [
		file
		for file in list(dir_service.list_directories_and_files())
		if not file["is_directory"]
		and re.match(filename.replace(".", r"\.").replace("*", ".*"), file["name"])
		is not None
	]

	for file in allfiles:
		with open(file["name"], "wb") as f:
			stream = ShareFileClient.from_connection_string(
				conn_str=connection_string,
				share_name="website",
				file_path=os.path.join(directory, file["name"]),
				offset=0,
				length=file["size"],
			).download_file()
			f.write(stream.readall())
	os.chdir(current_pwd)


def smb_put(filename):
	"""Put a file on the smb server.
	Directory to file on local machine will be used on server.
	"""
	directory, filename = os.path.split(filename)
	dir_service = ShareDirectoryClient.from_connection_string(
		conn_str=connection_string, share_name="website", directory_path=directory
	)

	with open(os.path.join(directory, filename), "rb") as f:
		dir_service.upload_file(
			file_name=filename,
			data=f,
			length=os.stat(os.path.join(directory, filename)).st_size,
		)


def smb_del(filename):
	"""Delete a file from the smb server."""
	directory, filename = os.path.split(filename)
	dir_service = ShareDirectoryClient.from_connection_string(
		conn_str=connection_string, share_name="website", directory_path=directory
	)

	dir_service.delete_file(file_name=filename)
