import pathlib

import plugin_server

from flask import (
	Flask,
	request,
	session,
	redirect,
	url_for,
	render_template
)
from flask_sitemap import Sitemap

secure_folder = pathlib.Path(__file__).parent / 'secure'

app = Flask(__name__, static_url_path='', static_folder='static')

app.config['TEMPLATES_AUTO_RELOAD'] = True

app.config['SITEMAP_INCLUDE_RULES_WITHOUT_PARAMS'] = True
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = 'iicalcnoreply@gmail.com'

with open(secure_folder / 'mail_password.txt') as f:
	app.config['MAIL_PASSWORD'] = f.read().strip()
with open(secure_folder / 'app_key.txt', 'rb') as f:
	app.secret_key = f.read().strip()

app.register_blueprint(plugin_server.app)

sitemap = Sitemap(app=app)


@app.route('/')
def homepage():
	print(app.config['TEMPLATES_AUTO_RELOAD'])
	return render_template('index.html')