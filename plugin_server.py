import os
import re
import json
from configparser import ConfigParser
import pathlib
import time
import sys
import io
import glob

import color_conversion
import smb_utils

import requests
from requests_oauthlib import OAuth2Session
import pkg_resources
import oauthlib

from flask_mail import Message, Mail
from flask import (
	render_template,
	request,
	redirect,
	make_response,
	Blueprint,
	send_file,
	current_app
)

parent_directory = pathlib.Path(__file__).parent
secure_folder = parent_directory / 'secure'

# setup flask stuff and declare static variables
app = Blueprint('plugin_server', __name__, template_folder=parent_directory / 'plugin_server_templates')

if not os.path.exists('tempplugins'):
	os.mkdir('tempplugins')
if not os.path.exists('plugins'):
	os.mkdir('plugins')
if not os.path.exists('ratings'):
	os.mkdir('ratings')

@app.route("/iicalc/plugins/index")
def pluginIndex():
	# User plugins refresh
	try:
		for file in os.listdir("tempplugins"):
			os.remove("tempplugins/" + file)
	except FileNotFoundError:
		pass

	try:
		for file in os.listdir("ratings"):
			os.remove("ratings/" + file)
	except FileNotFoundError:
		pass

	smb_utils.smb_get("tempplugins/*", "tempplugins")
	# Utility files refresh
	os.chdir("plugins")
	files = glob.glob("*.txt")

	for file in files:
		os.remove(file)
	smb_utils.smb_get("plugins/*")
	os.chdir("..")
	smb_utils.smb_get("ratings/*", "ratings")
	merge()
	return send_file("plugins/index.ini")


def getIndex():
	try:
		for file in os.listdir("tempplugins"):
			os.remove("tempplugins/" + file)
	except FileNotFoundError:
		pass

	smb_utils.smb_get("tempplugins/*", "tempplugins")
	merge()
	with open("plugins/index.ini") as indexFile:
		return indexFile.read()


def getAdmins():
	adminIds = []
	with open(secure_folder / "admins.txt") as f:
		adminNames = [line.rstrip() for line in f.readlines()]
	for user in adminNames:
		adminIds.append(
			str(
				json.loads(requests.get("https://api.github.com/users/" + user).text)[
					"id"
				]
			)
		)
	return adminIds


def listToSentance(listOfTerms):
	if len(listOfTerms) == 1:
		return listOfTerms[0]
	elif len(listOfTerms) == 2:
		return listOfTerms[0] + " and " + listOfTerms[1]
	else:
		commaList = ""
		for i in range(len(listOfTerms)):
			commaList = commaList + listOfTerms[i] + ", "
		commaList = commaList.strip(", ")
		commaList = ", and ".join(commaList.rsplit(", ", 1))
		return commaList


def sendDeleteEmail(name, reason, ban):
	os.chdir("plugins")
	if "emails.ini" in os.listdir("."):
		os.remove("emails.ini")
	smb_utils.smb_get("plugins/emails.ini")
	os.chdir("..")
	config = ConfigParser()
	config.read("plugins/index.ini")
	emaillist = ConfigParser()
	emaillist.read("plugins/emails.ini")
	users = config[name]["maintainer"].split(",")
	emails = []
	usernames = []
	for i in range(len(users)):
		user = json.loads(requests.get("https://api.github.com/user/" + users[i]).text)
		if emaillist.has_option("emails", users[i]):
			emails.append(emaillist["emails"][users[i]])
			usernames.append(str(user["login"]))
	emails = list(filter(("null").__ne__, emails))
	emails = list(filter(("None").__ne__, emails))
	if len(emails) > 0:
		msg = Message(
			subject="Your plugin, " + str(name) + ", was removed",
			sender="iicalcnoreply@gmail.com",
			recipients=emails,
		)
		body = (
			"Hi "
			+ listToSentance(usernames)
			+ ",\nYour plugin, "
			+ str(name)
			+ ", has been recently removed from the plugin store for the following reason: \n\n\t"
			+ reason
			+ "\n\n"
		)
		if ban:
			body += "This has resulted in a permanent ban from the store. You will no longer be able to create plugins, update plugins, delete plugins, or other related things."
		else:
			body += "This has resulted in a warning. You may resubmit your plugins after updating them to fit the guidelines which can be found here: https://github.com/TurboWafflz/ImaginaryInfinity-Calculator/blob/master/guidelines.md. Subsequent offenses may result in a permanent ban."
		msg.body = body
		msg.html = render_template(
			"pluginDeletedEmail.html",
			reason=reason,
			name=name,
			ban=ban,
			users=listToSentance(usernames),
		)
		with current_app.app_context():
			mail = Mail()
			mail.send(msg)


def sendMaintainerEmail(pluginName, adderName, maintainers, remove=False):
	if len(maintainers["maintainers"]) == 0:
		return
	for maintainer in maintainers["maintainers"]:
		maintainer = maintainer[0]
		email = maintainer[1]

		if not remove:
			url = "https://url.com"
			msg = Message(
				subject="Invitation to be a maintainer of " + str(pluginName),
				sender="iicalcnoreply@gmail.com",
				recipients=email,
			)
			msg.body = (
				adderName
				+ " has invited you to become a maintainer of the ImaginaryInfinity Calculator plugin, "
				+ str(pluginName)
				+ ". To accept the invitation, please copy and paste the link below into your browser. This link expires in 1 week. If you fail to accept within 1 week, please request a maintainer of this plugin to send you another invite. Thank you and have a nice day.\n\n"
				+ url
			)
			msg.html = render_template(
				"maintainerEmail.html",
				pluginName=pluginName,
				adderName=adderName,
				maintainer=maintainer,
				url=url,
				remove=False,
			)
			with current_app.app_context():
				mail = Mail()
				mail.send(msg)
		else:
			msg = Message(
				subject="You are no longer a maintainer of " + str(pluginName),
				sender="iicalcnoreply@gmail.com",
				recipients=email,
			)
			msg.body = (
				adderName
				+ " has removed you from being one of the maintainers of the ImaginaryInfinity Calculator plugin, "
				+ str(pluginName)
				+ ". If you have any concerns, please email turbowafflz@gmail.com or tabulatejarl8@gmail.com for futher support. Thank you and have a nice day."
			)
			msg.html = render_template(
				"maintainerEmail.html",
				pluginName=pluginName,
				adderName=adderName,
				maintainer=maintainer,
				remove=True,
			)
			with current_app.app_context():
				mail = Mail()
				mail.send(msg)


@app.route("/iicalc/auth")
def githubAuth():
	with open(secure_folder / 'github_oauth_client_id.txt') as f:
		client_id = f.read().strip()
	authorization_base_url = "https://github.com/login/oauth/authorize"

	github = OAuth2Session(client_id, scope="user:email")

	# Redirect user to GitHub for authorization
	authorization_url, state = github.authorization_url(authorization_base_url)
	response = make_response(redirect(authorization_url))
	if "connectCalc" in request.args:
		if "authToken" in request.cookies:
			return render_template(
				"copyToken.html", token=request.cookies.get("authToken")
			)
		response.set_cookie("connectCalc", "true", max_age=60 * 60 * 24)
	return response


@app.route("/iicalc/authCallback")
def authCallback():
	try:
		github = OAuth2Session("585f2125c38af29142b8", scope="user:email")

		with open(secure_folder / 'github_oauth_client_secret.txt') as f:
			client_secret = f.read().strip()

		newToken = github.fetch_token(
			"https://github.com/login/oauth/access_token",
			client_secret=client_secret,
			authorization_response=request.url.replace("http://", "https://"),
		)
		newToken = json.loads(str(newToken).replace("'", '"'))["access_token"]
		r = json.loads(github.get("https://api.github.com/user").text)
	except oauthlib.oauth2.rfc6749.errors.MissingCodeError:
		return render_template("oautherror.html"), 400
	except oauthlib.oauth2.rfc6749.errors.CustomOAuth2Error:
		return render_template("oautherror.html"), 401
	if "message" in r:
		return render_template("oautherror.html"), 401
	else:
		if "emails.ini" in os.listdir("plugins/"):
			os.remove("plugins/emails.ini")
			os.chdir("plugins")
		smb_utils.smb_get("plugins/emails.ini")
		emails = ConfigParser()
		emails.read("emails.ini")
		if not emails.has_option("emails", str(r["id"])):
			emails["emails"][str(r["id"])] = r["email"]
			with open("emails.ini", "w+") as f:
				emails.write(f)
			smb_utils.smb_put("plugins/emails.ini")
		elif emails["emails"][str(r["id"])] != r["email"]:
			emails["emails"][str(r["id"])] = r["email"]
			with open("emails.ini", "w+") as f:
				emails.write(f)
			smb_utils.smb_put("plugins/emails.ini")
		os.chdir("..")
		if "connectCalc" in request.cookies:
			response = make_response(render_template("copyToken.html", token=newToken))
			response.set_cookie("authToken", newToken, max_age=90 * 60 * 60 * 24)
			response.set_cookie("connectCalc", "", expires=0)
			return response
		else:
			response = make_response(
				redirect(
					"https://turbowafflz.gitlab.io/indexportal.html?authToken="
					+ newToken
				)
			)
			response.set_cookie("authToken", newToken, max_age=90 * 60 * 60 * 24)
			return response


def merge():
	# print(time.ctime().replace("  ", " ") + ": Merging Plugins into Index...")
	index = ConfigParser()
	# index.read("./plugins/index.ini")
	try:
		os.remove("plugins/index.ini")
	except FileNotFoundError:
		pass

	indexsections = index.sections()
	for file in os.listdir("tempplugins"):
		print(file)
		pluginconfig = ConfigParser()
		pluginconfig.read(os.path.join("tempplugins", file))
		pluginname = pluginconfig.sections()[0]
		if pluginname not in indexsections:
			# appending plugin
			print(pluginname)
			index.add_section(pluginname)

		for (key, item) in pluginconfig.items(pluginname):
			index[pluginname][key] = item

		# Calculate ratings
		pluginconfig.read(os.path.join("ratings", pluginname + ".ini"))
		rating = len(pluginconfig["upvotes"]) - len(pluginconfig["downvotes"])
		index[pluginname]["rating"] = str(rating)

	with open("./plugins/index.ini", "w+") as f:
		print("Writing file...")
		index.write(f)


@app.route("/iicalc/uploadplugin", methods=["GET", "POST"])
def uploadplugin():
	# return "This feature is temporarily disabled"
	pluginIndex()
	if request.method == "POST":
		type = request.form["type"]
		print(type)
		name = request.form["name"].strip()
		print(name)
		f = request.form["fileUrl"].strip()
		print(f)
		description = request.form["description"].strip()
		print(description)
		summary = request.form["summary"].strip()
		print(summary)
		version = request.form["version"].strip()
		print(version)
		depends = request.form["depends"].strip()
		print(depends)
		filename = request.form["fileName"].strip()
		print(filename)
		filehash = request.form["fileHash"].strip()
		print(filehash)

		# Parse versions to make sure its valid
		try:
			calcversion = re.sub(
				r"^[^>=<!~]*", "", request.form["calcversion"].strip().replace(" ", "")
			)
			for versionnum in calcversion.split(","):
				if (
					re.match(
						r"""
						^
						(~=|==|!=|<=|>=|<|>|===){1}    # match a comparison operator
						([1-9][0-9]*!)?                # ask PEP-0440, I have no clue
						(0|[1-9][0-9]*)                # https://www.python.org/dev/peps/pep-0440/#appendix-b-parsing-version-strings-with-regular-expressions
						(\.(0|[1-9][0-9]*))*
						((a|b|rc)(0|[1-9][0-9]*))?
						(\.post(0|[1-9][0-9]*))?
						(\.dev(0|[1-9][0-9]*))?
						$
						""",
						versionnum,
					)
					is None
				):
					raise pkg_resources.extern.packaging.requirements.InvalidRequirement
			if len(pkg_resources.Requirement.parse("iicalc" + calcversion).specs) == 0:
				raise pkg_resources.extern.packaging.requirements.InvalidRequirement
		except pkg_resources.extern.packaging.requirements.InvalidRequirement:
			return "Invlaid version format", 400

		maintainernames = (
			request.form["maintainers"]
			.replace(" ", "")
			.replace("\n", "")
			.replace("\t", "")
			.replace("\r", "")
			.split(",")
		)
		while "" in maintainernames:
			maintainernames.remove("")
		with open("plugins/bannedUsers.txt") as bannedFile:
			bannedUsers = [line.rstrip() for line in bannedFile.readlines()]

		if "authToken" in request.cookies:
			r = json.loads(
				requests.get(
					"https://api.github.com/user",
					headers={
						"Authorization": "Bearer "
						+ str(request.cookies.get("authToken"))
					},
				).text
			)
		else:
			return redirect("/iicalc/auth")
		if "message" in r:
			return render_template("oautherror.html"), 401
		elif r["login"] in bannedUsers:
			return render_template("banned.html"), 200
		else:
			currentIndex = ConfigParser()
			currentIndex.read("plugins/index.ini")

			blacklist = [
				'Search',
				'Updates',
				'Installed'
			]

			if name in currentIndex.sections() or name in blacklist:
				return render_template("nameInUse.html"), 400
			else:
				newparser = ConfigParser()
				newparser.add_section(name)
				newparser[name]["description"] = description
				maintainernames = list(
					dict.fromkeys(maintainernames)
				)  # remove duplicates
				maintainers = []
				for user in maintainernames:
					response = json.loads(
						requests.get(
							"https://api.github.com/users/" + user,
							headers={
								"Authorization": "Bearer "
								+ str(request.cookies.get("authToken"))
							},
						).text
					)
					try:
						maintainers.append(str(response["id"]))
					except KeyError:
						return render_template("userNotFound.html"), 400

				if not str(r["id"]) in maintainers:
					maintainers.append(str(r["id"]))
				newparser[name]["maintainer"] = ",".join(maintainers)
				newparser[name]["version"] = version
				newparser[name]["download"] = f
				newparser[name]["hash"] = filehash
				newparser[name]["lastupdate"] = str(time.time())
				newparser[name]["summary"] = summary
				newparser[name]["filename"] = filename
				if len(depends) > 0:
					newparser[name]["depends"] = depends
				newparser[name]["type"] = type
				newparser[name]["approved"] = "false"
				newparser[name]["reported"] = "0"
				newparser[name]["lastUpdater"] = str(r["login"])
				newparser[name]["owner"] = str(r["login"])
				newparser[name]["calcversion"] = "iicalc" + calcversion

				if not os.path.exists("./tempplugins"):
					os.mkdir("./tempplugins")
				if not os.path.exists("./ratings"):
					os.mkdir("./ratings")

				smb_utils.smb_get(
					"tempplugins/*", "tempplugins"
				)  # Download all of tempplugins to local tempplugins directory
				smb_utils.smb_get("ratings/*", "ratings")
				with open("./tempplugins/" + name + ".ini", "w+") as file:
					newparser.write(file)
				with open("./ratings/" + name + ".ini", "w+") as file:
					file.write("[upvotes]\n\n[downvotes]\n")
				smb_utils.smb_put("tempplugins/" + name + ".ini")
				smb_utils.smb_put("ratings/" + name + ".ini")

				return render_template("pluginsubmitsuccess.html"), 200


@app.route("/iicalc/updateplugin", methods=["GET", "POST"])
def updateplugin():
	# return "This feature is temporarily disabled"
	pluginIndex()
	if request.method == "POST":
		name = request.form["name"].strip()
		f = request.form["fileUrl"].strip()
		description = request.form["description"].strip()
		summary = request.form["summary"].strip()
		version = request.form["version"].strip()
		depends = request.form["depends"].strip()
		filehash = request.form["fileHash"].strip()

		# Parse versions to make sure its valid
		try:
			calcversion = re.sub(
				r"^[^>=<!~]*", "", request.form["calcversion"].strip().replace(" ", "")
			)
			for versionnum in calcversion.split(","):
				if (
					re.match(
						r"""
						^
						(~=|==|!=|<=|>=|<|>|===){1}    # match a comparison operator
						([1-9][0-9]*!)?                # ask PEP-0440, I have no clue
						(0|[1-9][0-9]*)                # https://www.python.org/dev/peps/pep-0440/#appendix-b-parsing-version-strings-with-regular-expressions
						(\.(0|[1-9][0-9]*))*
						((a|b|rc)(0|[1-9][0-9]*))?
						(\.post(0|[1-9][0-9]*))?
						(\.dev(0|[1-9][0-9]*))?
						$
						""",
						versionnum,
					)
					is None
				):
					raise pkg_resources.extern.packaging.requirements.InvalidRequirement
			if len(pkg_resources.Requirement.parse("iicalc" + calcversion).specs) == 0:
				raise pkg_resources.extern.packaging.requirements.InvalidRequirement
		except pkg_resources.extern.packaging.requirements.InvalidRequirement:
			return "Invlaid version format", 400
		with open("plugins/bannedUsers.txt") as bannedFile:

			bannedUsers = [line.rstrip() for line in bannedFile.readlines()]
		if "authToken" in request.cookies:
			r = json.loads(
				requests.get(
					"https://api.github.com/user",
					headers={
						"Authorization": "Bearer "
						+ str(request.cookies.get("authToken"))
					},
				).text
			)
		else:
			return redirect("/iicalc/auth")

		currentIndex = ConfigParser()
		currentIndex.read("plugins/index.ini")
		if "message" in r:
			return "<h1>Unauthorized</h1>", 401
		elif r["login"] in bannedUsers:
			return render_template("banned.html"), 200
		elif name not in currentIndex.sections():
			return render_template("pluginNotFound.html"), 400
		elif not str(r["id"]) in str(currentIndex[name]["maintainer"]).split(","):
			return render_template("wrongMaintainer.html", role="maintainer"), 401
		else:
			newparser = ConfigParser()
			newparser.read("./tempplugins/" + name + ".ini")
			if len(description) != 0:
				newparser[name]["description"] = description
			newparser[name]["version"] = version
			if len(f) != 0:
				newparser[name]["download"] = f
			newparser[name]["hash"] = filehash
			newparser[name]["lastupdate"] = str(time.time())
			if len(summary) != 0:
				newparser[name]["summary"] = summary
			if depends == "":
				if newparser.has_option(name, "depends"):
					newparser.remove_option(name, "depends")
			else:
				newparser[name]["depends"] = depends
			newparser[name]["approved"] = "false"
			newparser[name]["lastUpdater"] = str(r["login"])

			newparser[name]["calcversion"] = "iicalc" + calcversion
			smb_utils.smb_get("tempplugins/*", "tempplugins")
			if not os.path.exists("./tempplugins"):
				os.mkdir("./tempplugins")
			with open("./tempplugins/" + name + ".ini", "w+") as file:
				newparser.write(file)
			smb_utils.smb_put("tempplugins/" + name + ".ini")
			return render_template("pluginsubmitsuccess.html"), 200


@app.route("/template/<template>")
def returnTemplate(template):
	return render_template(template)


@app.route("/iicalc/fillbutton", methods=["GET", "POST"])
def fill():
	print("Autofill")
	try:
		for file in os.listdir("tempplugins"):
			os.remove("tempplugins/" + file)
	except FileNotFoundError:
		pass
	smb_utils.smb_get("tempplugins/*", "tempplugins")
	merge()
	pluginname = request.form["pluginname"]
	index = ConfigParser()
	index.read("./plugins/index.ini")
	print(index.sections(), file=sys.stderr, flush=True)
	if pluginname not in index.sections():
		return "false"
	else:

		if index.has_option(pluginname, "depends"):
			data = {
				"download": index[pluginname]["download"],
				"description": index[pluginname]["description"],
				"summary": index[pluginname]["summary"],
				"version": index[pluginname]["version"],
				"depends": index[pluginname]["depends"],
				"calcversion": index[pluginname]["calcversion"],
			}
		else:
			data = {
				"download": index[pluginname]["download"],
				"description": index[pluginname]["description"],
				"summary": index[pluginname]["summary"],
				"version": index[pluginname]["version"],
				"depends": "",
				"calcversion": index[pluginname]["calcversion"],
			}
		return data


@app.route("/iicalc/browse")
def pluginBrowser():
	pluginIndex()
	index = ConfigParser()
	index.read("plugins/index.ini")
	# print(index.sections())
	# print(index["bgm"])
	return render_template("pluginBrowser.html", index=index._sections)


@app.route("/iicalc/viewplugin/<plugin>")
def viewPlugin(plugin):
	index = ConfigParser()
	index.read("plugins/index.ini")
	ratings = ConfigParser()
	ratings.read("ratings/" + plugin + ".ini")
	lastUpdate = index[plugin]["lastUpdate"]
	lastUpdate = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(lastUpdate)))
	maintainerIds = index[plugin]["maintainer"].split(",")
	maintainers = []
	maintainernames = {"names": []}
	for id in maintainerIds:
		# print(id)
		if "authToken" in request.cookies:
			response = json.loads(
				requests.get(
					"https://api.github.com/user/" + id,
					headers={
						"Authorization": "Bearer "
						+ str(request.cookies.get("authToken"))
					},
				).text
			)
			if "message" in response:
				response = json.loads(
					requests.get("https://api.github.com/user/" + id).text
				)
		else:
			response = json.loads(
				requests.get("https://api.github.com/user/" + id).text
			)
		maintainers.append([response["login"], response["avatar_url"]])
		maintainernames["names"].append(response["login"])

	# Check if maintainer editor should be shown
	isMaintainer = False
	if "authToken" in request.cookies:
		r = json.loads(
			requests.get(
				"https://api.github.com/user",
				headers={
					"Authorization": "Bearer " + str(request.cookies.get("authToken"))
				},
			).text
		)
		if "message" not in r:
			if (
				str(r["id"])
				in str(index[plugin]["maintainer"]).split(",") + getAdmins()
			):
				isMaintainer = True

	# check if delete button should be shown
	delete = False
	if "authToken" in request.cookies:
		if "message" not in r:
			with open(secure_folder / "admins.txt") as f:
				if str(r["login"]) in [index[plugin]["owner"]] + [
					line.rstrip() for line in f.readlines()
				]:
					delete = True

	# Check if user has voted
	voted = {"up": False, "down": False}
	if "authToken" in request.cookies:
		if "message" not in r:
			if str(r["id"]) in ratings["upvotes"]:
				voted["up"] = True
			elif str(r["id"]) in ratings["downvotes"]:
				voted["down"] = True

	# Calulator votes
	votes = str(len(ratings["upvotes"]) - len(ratings["downvotes"]))

	# Theme Preview
	if index[plugin]["type"] == "themes":
		themeFile = requests.get(index[plugin]["download"])
		themeConfig = ConfigParser()
		if themeFile.status_code == 200:
			file = io.StringIO()
			file.write(themeFile.content.decode())
			file.seek(0)
			themeConfig.read_file(file)
			themeColorsHex = {}
			if themeConfig["theme"]["ansi"] == "false":
				for themeDef in themeConfig["styles"]:
					themeColorsHex[themeDef] = color_conversion.ansi_to_hex(
						color_conversion.colorama_to_ansi(themeConfig["styles"][themeDef])
					)
			else:
				for themeDef in themeConfig["styles"]:
					themeColorsHex[themeDef] = color_conversion.ansi_to_hex(
						themeConfig["styles"][themeDef]
					)

			defaults = {
				"normal": "#d3d7cf",
				"error": "#cc0000",
				"warning": "#c4a000",
				"important": "#75507b",
				"startupmessage": "#c4a000",
				"prompt": "#4e9a06",
				"link": "#3465a4",
				"answer": "#4e9a06",
				"input": "#06989a",
				"output": "#d3d7cf",
			}

			for default in defaults:
				if default not in themeColorsHex:
					themeColorsHex[default] = {}
					themeColorsHex[default]["fore"] = defaults[default]
					themeColorsHex[default]["back"] = ""

			showTerminal = True
	else:
		themeColorsHex = {}
		showTerminal = False

	return render_template(
		"viewPlugin.html",
		pluginName=plugin,
		plugin=index[plugin],
		lastUpdate=lastUpdate,
		maintainers=maintainers,
		isMaintainer=isMaintainer,
		delete=delete,
		maintainernames=maintainernames,
		voted=voted,
		votes=votes,
		theme=themeColorsHex,
		showTerminal=showTerminal,
	)


@app.route("/iicalc/search", methods=["GET", "POST"])
def pluginSearch():
	searchTerm = request.form["searchTerm"]
	authors = list(set([x[7:] for x in searchTerm.split() if x.startswith("author:")]))
	authorids = []
	for user in authors:
		try:
			if "authToken" in request.cookies:
				id = json.loads(
					requests.get(
						"https://api.github.com/users/" + user,
						headers={
							"Authorization": "Bearer "
							+ str(request.cookies.get("authToken"))
						},
					).text
				)
				if "message" in id:
					authorids.append(
						str(
							json.loads(
								requests.get(
									"https://api.github.com/users/" + user
								).text
							)["id"]
						)
					)
				else:
					authorids.append(str(id["id"]))
			else:
				authorids.append(
					str(
						json.loads(
							requests.get("https://api.github.com/users/" + user).text
						)["id"]
					)
				)
		except Exception:
			# im not really sure what I'm meant to be catching here since i
			# wrote this a very long time ago
			pass
	types = list(
		set(
			[
				x[5:].rstrip("s") + "s"
				for x in searchTerm.split()
				if x.startswith("type:")
				and x[5:] in ["plugin", "plugins", "theme", "themes"]
			]
		)
	)

	# print(searchTerm)
	index = ConfigParser()
	index.read("plugins/index.ini")
	if len(authorids) != 0:
		for plugin in index.sections():
			if set(authorids).isdisjoint(index[plugin]["maintainer"].split(",")):
				index.remove_section(plugin)

	if len(types) != 0:
		for plugin in index.sections():
			if not index[plugin]["type"] in types:
				index.remove_section(plugin)

	searchTerm = " ".join(
		word
		for word in searchTerm.split()
		if not word.startswith("type:") and not word.startswith("author:")
	)
	# print(index.sections())
	# print(index["bgm"])
	return render_template(
		"pluginSearch.html", index=index._sections, searchTerm=searchTerm
	)


@app.route("/iicalc/admin/screening")
def screen():
	pluginIndex()
	if "authToken" in request.cookies:
		r = json.loads(
			requests.get(
				"https://api.github.com/user",
				headers={
					"Authorization": "Bearer " + str(request.cookies.get("authToken"))
				},
			).text
		)
	else:
		return redirect("/iicalc/auth")
	if "message" in r:
		return "<h1>Unauthorized</h1>", 401
	elif not str(r["id"]) in getAdmins():
		return "Forbidden", 403
	else:
		index = ConfigParser()
		index.read("plugins/index.ini")
		unscreened = []
		for name in index.sections():
			if index[name]["approved"] == "false":
				unscreened.append(
					{
						"name": name,
						"type": index[name]["type"][:1].upper()
						+ index[name]["type"][1:-1],
						"badge": index[name]["type"][:-1] + "Badge",
						"version": index[name]["version"],
						"summary": index[name]["summary"],
					}
				)
		return render_template("screen.html", unscreened=unscreened)


@app.route("/iicalc/admin/approve", methods=["GET", "POST"])
def approve():
	if request.method == "POST":
		pluginIndex()
		name = request.form["name"]
		if "authToken" in request.cookies:
			r = json.loads(
				requests.get(
					"https://api.github.com/user",
					headers={
						"Authorization": "Bearer "
						+ str(request.cookies.get("authToken"))
					},
				).text
			)
		else:
			return redirect("/iicalc/auth")
		if "message" in r:
			return "<h1>Unauthorized</h1>", 401
		elif not str(r["id"]) in getAdmins():
			return "Forbidden", 403
		else:
			index = ConfigParser()
			index.read("./tempplugins/" + name + ".ini")
			index[name]["approved"] = "true"
			index[name]["reported"] = "0"
			with open("./tempplugins/" + name + ".ini", "w+") as f:
				index.write(f)
			try:
				os.remove("./plugins/" + name + ".txt")
				smb_utils.smb_put("plugins/" + name + ".txt")
			except FileNotFoundError:
				pass
			smb_utils.smb_put("tempplugins/" + name + ".ini")
			return "true", 200


@app.route("/iicalc/deleteplugin", methods=["GET", "POST"])
def deleteplugin():
	# return "This feature is temporarily disabled"
	pluginIndex()
	if request.method == "POST":
		name = request.form["name"]
		with open("plugins/bannedUsers.txt") as bannedFile:
			bannedUsers = [line.rstrip() for line in bannedFile.readlines()]
		reason = None
		if "reason" in request.form:
			reason = request.form["reason"]

		if "authToken" in request.cookies:
			r = json.loads(
				requests.get(
					"https://api.github.com/user",
					headers={
						"Authorization": "Bearer "
						+ str(request.cookies.get("authToken"))
					},
				).text
			)
		else:
			return redirect("/iicalc/auth")
		if "message" in r:
			return render_template("oautherror.html"), 401
		elif r["login"] in bannedUsers:
			return render_template("banned.html"), 200
		else:
			currentIndex = ConfigParser()
			currentIndex.read("plugins/index.ini")
			if not str(r["id"]) in [currentIndex[name]["owner"]] + getAdmins():
				return render_template("wrongMaintainer.html", role="owner"), 401
			else:
				if reason is not None:
					bannables = [
						"Malicious",
						"Promotion of terrorism",
						"Revealing someone's personal of confidential information",
						"Illegal content",
						"Threats",
					]
					ban = False
					if "ban" in request.form:
						if request.form["ban"] == "true":
							ban = True
					if reason in bannables:
						ban = True
					# test warnings
					warnings = ConfigParser()
					warnings.read("plugins/warnings.ini")
					if not ban:
						if warnings.has_option(
							"users", currentIndex[name]["lastUpdater"]
						):
							ban = True
						elif currentIndex[name]["lastUpdater"] not in getAdmins():
							warnings["users"][
								currentIndex[name]["lastUpdater"]
							] = "true"
							with open("plugins/warnings.ini", "w+") as warnfile:
								warnings.write(warnfile)
							smb_utils.smb_put("plugins/warnings.ini")
					if currentIndex[name]["lastUpdater"] in getAdmins():
						ban = False
					try:
						sendDeleteEmail(name, reason, ban)
					except Exception:
						# any exceptions during email sending
						pass
					os.remove("./tempplugins/" + name + ".ini")

					if ban:
						if not currentIndex[name]["lastUpdater"] in bannedUsers:
							with open("plugins/bannedUsers.txt", "a+") as f:
								f.write(currentIndex[name]["lastUpdater"] + "\n")
							smb_utils.smb_put("plugins/bannedUsers.txt")
						os.remove("./ratings/" + name + ".ini")
						smb_utils.smb_del("ratings/" + name + ".ini")
					try:
						os.remove("./plugins/" + name + ".txt")
						smb_utils.smb_del("plugins/" + name + ".txt")
					except FileNotFoundError:
						pass

					smb_utils.smb_del("tempplugins/" + name + ".ini")
					return "Success", 200

					# return "true", 200
				else:
					os.remove("./tempplugins/" + name + ".ini")
					os.remove("./ratings/" + name + ".ini")
					try:
						os.remove("./plugins/" + name + ".txt")
						smb_utils.smb_del("plugins/" + name + ".txt")
					except FileNotFoundError:
						pass

					smb_utils.smb_del("tempplugins/" + name + ".ini")
					smb_utils.smb_del("ratings/" + name + ".ini")
					return render_template("plugindeletesuccess.html")


@app.route("/iicalc/getplugins", methods=["GET", "POST"])
def getplugins():
	# return "This feature is temporarily disabled"
	pluginIndex()
	if request.method == "POST":
		if "authToken" in request.cookies:
			r = json.loads(
				requests.get(
					"https://api.github.com/user",
					headers={
						"Authorization": "Bearer "
						+ str(request.cookies.get("authToken"))
					},
				).text
			)
		else:
			return "Invalid OAuth Session"
		if "message" in r:
			return "OAuth Error"
		else:
			currentIndex = ConfigParser()
			currentIndex.read("plugins/index.ini")
			userplugins = ""
			for section in currentIndex.sections():
				if str(currentIndex[section]["maintainer"]) == str(r["id"]):
					userplugins = userplugins + section + ","
			userplugins = userplugins.strip(",")
			return userplugins, 200


@app.route("/iicalc/reportplugin", methods=["GET", "POST"])
def reportplugin():
	pluginIndex()
	if request.method == "POST":
		name = request.form["name"]
		reason = request.form["reason"]
		if "authToken" in request.cookies:
			r = json.loads(
				requests.get(
					"https://api.github.com/user",
					headers={
						"Authorization": "Bearer "
						+ str(request.cookies.get("authToken"))
					},
				).text
			)
		else:
			return redirect("/iicalc/auth")
		if "message" in r:
			return render_template("oautherror.html"), 401
		else:
			currentIndex = ConfigParser()
			currentIndex.read("plugins/index.ini")
			if name in currentIndex.sections():
				pluginFile = ConfigParser()
				pluginFile.read("./tempplugins/" + name + ".ini")
				pluginFile[name]["approved"] = "false"
				pluginFile[name]["reported"] = str(
					int(pluginFile[name]["reported"]) + 1
				)
				with open("./tempplugins/" + name + ".ini", "w+") as f:
					pluginFile.write(f)
				with open("plugins/" + name + ".txt", "a+") as f:
					f.write(str(reason) + "\n")
				smb_utils.smb_put("tempplugins/" + name + ".ini")
				smb_utils.smb_put("plugins/" + name + ".txt")
				return render_template("pluginReportSuccess.html"), 200
			else:
				return render_template("pluginNotFound.html"), 400


@app.route("/iicalc/delete")
def deletepage():
	return render_template("delete.html")


@app.route("/iicalc/editmaintainers", methods=["GET", "POST"])
def editmaintainers():
	if request.method == "POST":
		name = request.form["name"]
		maintainernames = (
			request.form["maintainers"]
			.replace(" ", "")
			.replace("\n", "")
			.replace("\t", "")
			.replace("\r", "")
			.split(",")
		)
		maintainernames = list(dict.fromkeys(maintainernames))
		if len(maintainernames) == 0:
			return "too few maintainers"
		maintainerids = []
		if "authToken" in request.cookies:
			r = json.loads(
				requests.get(
					"https://api.github.com/user",
					headers={
						"Authorization": "Bearer "
						+ str(request.cookies.get("authToken"))
					},
				).text
			)
		else:
			return redirect("/iicalc/auth")
		if "message" in r:
			return render_template("oautherror.html"), 401
		else:
			if name + ".ini" in os.listdir("./tempplugins"):
				plugin = ConfigParser()
				plugin.read("./tempplugins/" + name + ".ini")
				# currentMaintainers = plugin[name]["maintainer"].split(",")
				if not plugin[name]["owner"] in maintainernames:
					maintainernames.append(plugin[name]["owner"])
				for maintainername in maintainernames:
					if "authToken" in request.cookies:
						id = json.loads(
							requests.get(
								"https://api.github.com/users/" + maintainername,
								headers={
									"Authorization": "Bearer "
									+ str(request.cookies.get("authToken"))
								},
							).text
						)
						if "message" in id:
							if id["message"] == "Bad credentials":
								id = json.loads(
									requests.get(
										"https://api.github.com/users/" + maintainername
									).text
								)
					else:
						id = json.loads(
							requests.get(
								"https://api.github.com/users/" + maintainername
							).text
						)
					if "message" not in id:
						maintainerids.append(str(id["id"]))
				# if not str(r["id"]) in maintainerids
				# check for owner?
				# removedMaintainersList = (list(list(set(currentMaintainers)-set(maintainerids))))
				# addedMaintainersList = ((list(set(maintainerids)-set(currentMaintainers))))
				# sendMaintainerEmail(name, r["login"], addedMaintainers, remove=False)
				# sendMaintainerEmail(name, r["login"], removedMaintainers, remove=True)
				plugin[name]["maintainer"] = ",".join(maintainerids)
				with open("./tempplugins/" + name + ".ini", "w+") as f:
					plugin.write(f)
				smb_utils.smb_put("tempplugins/" + name + ".ini")
				return render_template(
					"maintainersChangedSuccess.html", pluginPage=request.referrer
				)
			else:
				return render_template("pluginNotFound.html"), 400


@app.route("/iicalc/rate/<plugin>", methods=["GET", "POST"])
def rateplugin(plugin):
	if request.method == "POST":
		if "authToken" in request.cookies:
			r = json.loads(
				requests.get(
					"https://api.github.com/user",
					headers={
						"Authorization": "Bearer "
						+ str(request.cookies.get("authToken"))
					},
				).text
			)
		else:
			return "Unauthorized", 401
		if "message" in r:
			return "Unauthorized", 401
		else:
			# pluginIndex()
			if plugin + ".ini" in os.listdir("tempplugins/"):
				pluginRatings = ConfigParser()
				pluginRatings.read("ratings/" + plugin + ".ini")
				vote = int(request.form["vote"])
				if vote == 1:
					if str(r["id"]) in pluginRatings["upvotes"]:
						return "Already upvoted", 200
					elif str(r["id"]) in pluginRatings["downvotes"]:
						pluginRatings.remove_option("downvotes", str(r["id"]))
					pluginRatings["upvotes"][str(r["id"])] = "1"
					with open("ratings/" + plugin + ".ini", "w+") as f:
						pluginRatings.write(f)
					smb_utils.smb_put("ratings/" + plugin + ".ini")
					return "Success", 200
				elif vote == -1:
					if str(r["id"]) in pluginRatings["downvotes"]:
						return "Already downvoted", 200
					elif str(r["id"]) in pluginRatings["upvotes"]:
						pluginRatings.remove_option("upvotes", str(r["id"]))
					pluginRatings["downvotes"][str(r["id"])] = "-1"
					with open("ratings/" + plugin + ".ini", "w+") as f:
						pluginRatings.write(f)
					smb_utils.smb_put("ratings/" + plugin + ".ini")
					return "Success", 200
				else:
					return "Invalid vote type: '" + str(vote) + "'", 400
			else:
				return "Plugin '" + plugin + "' not found", 400
